# MI Classification Using ML and Deep Learning

This project is aimed to classify EEG based Motor Imagery signals based on *BCI Competition IV – Data sets 2a*. 

The project & code is heavily influenced by Pierce Stegman and his work on BCI.js

The data changed into CSV in the data folder is of subject 3. In case there is a need to test on other subject, the .m file used to export the CSV files from the .mat files of the dataset is uploaded as well - *SubjectDataToCSV.m*

As for the deep learning model based on CNN it is available in the CNN directory as an ipython jupyter notebook. The data needed for the notebook to work has to be mapped in a different way and hence in case you need the .m file to map the data and export the slice files feel free to contact me. The slice files are available [here](https://github.com/shariharan205/Motor-Imagery-Tasks-Classification-using-EEG-data/tree/master/data).